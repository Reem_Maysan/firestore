import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String imgUrl;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Image Storage'),
      ),
      body: Column(
        children: <Widget>[
          (imgUrl != null)
              ? Image.network(imgUrl)
              : Placeholder(
                  fallbackHeight: 200.0,
                  fallbackWidth: double.infinity,
                ),
          SizedBox(
            height: 20.0,
          ),
          RaisedButton(
            onPressed: () {
              uploadFile();
            },
            color: Colors.lightBlue,
            child: Text('Upload'),
          )
        ],
      ),
    );
  }

  uploadFile() async {
    final _picker = ImagePicker();
    final _storage=FirebaseStorage.instance;
    PickedFile image;
    await Permission.photos.request();
    var permissionState = await Permission.photos.status;
    if (permissionState.isGranted) {
      image = await _picker.getImage(source: ImageSource.gallery);
      var file = File(image.path);
      if (image != null) {
        var snapshot=await _storage.ref().child('folder/imgName').putFile(file).onComplete;
        var downloadedUrl= await snapshot.ref.getDownloadURL();
        setState(() {
         imgUrl=downloadedUrl; 
        });
      } else {
        print('no image path..try again!!');
      }
    } else {
      print('no permissio..try again!!');
    }
  }
}
